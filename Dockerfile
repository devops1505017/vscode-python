FROM ubuntu:24.04

RUN apt-get update
RUN apt-get install \
    nano \
    wget \
    pip \
    openssh-server \
    git \
    sudo \
    make \
    sshpass \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    software-properties-common \
    gcc -y \
    && add-apt-repository -y ppa:deadsnakes/ppa \
    && rm -rf /var/cache/apt
RUN apt install python3.13-full -y && rm -rf /var/cache/apt

RUN wget -O- https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor | sudo tee /etc/apt/keyrings/docker.gpg > /dev/null
RUN echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu jammy stable"| sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN sudo apt update
RUN sudo apt-get install docker-ce docker-ce-cli containerd.io -y && rm -rf /var/cache/apt

RUN userdel -r ubuntu
RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 dev
RUN echo 'dev:dev' | chpasswd
RUN service ssh start
EXPOSE 22
CMD ["/usr/sbin/sshd","-D"]

