## VSCode connection

.ssh/config
```
Host *
    StrictHostKeyChecking no

Host Python_3_12_git
    HostName 10.0.0.1
    Port 136
    User dev
    IdentityFile ~\.ssh\id_rsa
```
